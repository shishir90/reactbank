import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { Actions } from "react-native-router-flux";
import Auth0Lock from 'react-native-lock';

import credentials from '../../auth0-credentials';
let lock = new Auth0Lock(credentials);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

class Login extends React.Component {
  render(){
    return (
      <View style={styles.container}>
        { this.showLock() }
      </View>
    );
  }

  showLock() {
    lock.show({
      closable: false,
      authParams: {
        connection: 'Username-Password-Authentication',
        scope: 'openid email offline_access',
      },
      //connections: ["touchid"],
    }, (err, profile, token) => {
      if (err) {
        console.log(err);
        return;
      }
      Actions.Tabbar({ profile: profile });
    });
  }
}

export default Login;