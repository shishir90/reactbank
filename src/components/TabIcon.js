import React, { PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center',
  },
});

class TabIcon extends React.Component {
  render(){
    const { iconName, title, selected } = this.props;
    const color = selected ? '#8b3171' : '#58003e';
    return (
      <View style={styles.container}>
        <Icon style={{color: color}} name={iconName} size={30} />
        <Text style={{color: color, fontSize: 12}}>{title}</Text>
      </View>
    );
  }
}

TabIcon.propTypes = {
  selected: PropTypes.bool,
  title: PropTypes.string.isRequired,
  iconName: PropTypes.string.isRequired,
};

export default TabIcon;