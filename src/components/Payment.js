import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import Auth0Lock from 'react-native-lock';
import Button from 'react-native-button';

import credentials from '../../auth0-credentials';
let lock = new Auth0Lock(credentials);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#efeaee'
  },
  description: {
    fontSize: 20,
    textAlign: 'center',
    color: '#564c53'
  },
  buttonContainer: {
    marginTop: 20,
    padding:10,
    height:45,
    overflow:'hidden',
    borderRadius:4,
    backgroundColor: '#8b3171'
  },
  button: {
    fontSize: 20,
    color: 'white'
  }
});

class Payment extends React.Component {

  showLock() {
    lock.show({
      closable: true,
      authParams: {
        scope: 'step_up',
      },
      connections: ["sms"],
    }, (err, profile, token) => {
      if (err) {
        console.log(err);
        return;
      }
      //Actions.Tabbar({ profile: profile });
      console.log('stepped up!');
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.description}>Select a payee</Text>
        <Button
          containerStyle={styles.buttonContainer}
          style={styles.button}
          onPress={() => this.showLock()}>
          Make Payment
        </Button>
      </View>
    );
  }
}

Payment.propTypes = {
  profile: PropTypes.object,
};

export default Payment;
