import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  Modal
} from 'react-native';
import Auth0Lock from 'react-native-lock';
import Button from 'react-native-button';
import AccountOverview from './AccountOverview';
import Icon from 'react-native-vector-icons/MaterialIcons';

import credentials from '../../auth0-credentials';
let lock = new Auth0Lock(credentials);
import firebaseApp from '../util/firebaseApp';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 63,
    backgroundColor: '#efeaee'
  },
  accountType: {
    fontSize: 20,
    paddingBottom: 5,
  },
  balance: {
    fontSize: 20,
    marginTop: 25,
  },
  availableBalance: {
    fontSize: 14,
    marginTop: 5,
  },
  baseText: {
    fontFamily: 'Lato',
    color: '#564c53'
  },
  header: {
    flex: 1,
    padding: 10,
    borderBottomColor: '#d5d0d4',
    borderBottomWidth: 1
  },
  buttonContainer: {
    marginTop: 20,
    padding:10,
    height:45,
    overflow:'hidden',
    borderRadius:4,
    backgroundColor: '#8b3171'
  },
  buttonContainerDisabled: {
    backgroundColor: 'grey'
  },
  button: {
    fontSize: 20,
    color: 'white'
  }
});

class Transfer extends React.Component {

  constructor(props) {
    super(props);
    const { account } = this.props;

    firebaseApp.database().ref().child('accounts/aielondon').once("value", (snap) => {
      // get children as an array
      var items = [];
      snap.forEach((child) => {
        if ((child.val().type == 'Credit card'
          || child.val().type == 'Current account'
          || child.val().type == 'Savings account')
          && child.val().number != account.number) {
          items.push({
            type: child.val().type,
            name: child.val().name,
            number: child.val().number,
            sort_code: child.val().sort_code,
            current_balance: child.val().current_balance,
            available_balance: child.val().available_balance,
            _key: child.key
          });
        }
      });

      this.state = {
        amount: null,
        toAccount: null,
        buttonDisabled: true,
        transferSuccess: false,
        modalVisible: false,
        availableTransferAccounts: items
      };
    });
  }

  showLock() {
    /*lock.show({
      closable: true,
      authParams: {
        scope: 'step_up',
      },
      connections: ["sms"],
    }, (err, profile, token) => {
      if (err) {
        this.setState({transferSuccess: false});
        return;
      }
      this.performTransfer();
    });*/
    this.performTransfer();
  }

  performTransfer() {
    // Push the transaction updates to firebase.
    const { account } = this.props;
    const { amount, toAccount } = this.state;
    const fromAccountRef = firebaseApp.database().ref(`transactions/${account.number}`);
    const toAccountRef = firebaseApp.database().ref(`transactions/${toAccount.number}`);
    fromAccountRef.push({
      'amount': parseFloat(amount),
      'transactionType': 'debit',
      'type': 'transfer',
      'name': 'Internal Transfer',
      'reference':
      `Transfer to ${toAccount.type}`,
      'date': Date.now()
    });
    toAccountRef.push({
      'amount': parseFloat(amount),
      'transactionType': 'credit',
      'name': 'Internal Transfer',
      'type': 'transfer',
      'reference': `Transfer from ${account.type}`,
      'date': Date.now()
    });

    // Update the balance for both accounts.
    this.debitBalance(account, parseFloat(amount));
    this.creditBalance(toAccount, parseFloat(amount));
    this.setState({transferSuccess: true});
  }

  debitBalance(account, amount) {
    let { current_balance, available_balance } = account;
    const newCurrentBalance = current_balance - amount;
    const newAvailableBalance = available_balance - amount;
    let update = {};
    update[`accounts/aielondon/${account.number}/current_balance`] = newCurrentBalance;
    update[`accounts/aielondon/${account.number}/available_balance`] = newAvailableBalance;
    firebaseApp.database().ref().update(update);
  }

  creditBalance(account, amount) {
    let { current_balance, available_balance } = account;
    const newCurrentBalance = current_balance + amount;
    const newAvailableBalance = available_balance + amount;
    let update = {};
    update[`accounts/aielondon/${account.number}/current_balance`] = newCurrentBalance;
    update[`accounts/aielondon/${account.number}/available_balance`] = newAvailableBalance;
    firebaseApp.database().ref().update(update);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  selectAccountFromModal(account) {
    let buttonDisabled = true;
    if (this.state.amount && this.state.amount != '') {
      buttonDisabled = false;
    }
    this.setState({
      toAccount: account,
      modalVisible: false,
      buttonDisabled: buttonDisabled
    });
  }

  setAmount(amount) {
    let buttonDisabled = true;
    if (this.state.toAccount && amount != '') {
      buttonDisabled = false;
    }
    this.setState({
      amount: amount,
      buttonDisabled: buttonDisabled
    });
  }

  renderTransfer() {
    const { transferSuccess, amount, toAccount } = this.state;
    if (transferSuccess) {
      return (
        <View style={{flex: 2, padding: 50, justifyContent: 'flex-start'}}>
          <Icon name="done" size={50} />
          <Text style={{fontSize: 20}}>Transfer Done</Text>
          <Text>{this.renderBalance(parseFloat(amount))} has been transferred to your {toAccount.type}</Text>
        </View>
      )
    }
  }

  renderMainView() {
    const { transferSuccess } = this.state;
    const _this = this;
    const { account } = this.props;
    const { buttonDisabled } = this.state;
    let buttonContainerStyle = [
      styles.buttonContainer,
      buttonDisabled ? styles.buttonContainerDisabled : null,
    ];

    if (!transferSuccess) {
      return (
        <View style={{flex: 3, padding: 10, justifyContent: 'flex-start'}}>
          <View>
            <Modal
            animationType={"slide"}
            transparent={false}
            visible={this.state.modalVisible}
            >
              {this.state.availableTransferAccounts.map(function(account) {
                return (
                  <View key={account.number} style={{padding: 10, borderBottomColor: '#d5d0d4', borderBottomWidth: 1}}>
                    <AccountOverview account={account} onPress={() => {
                    _this.selectAccountFromModal(account)
                    }}/>
                  </View>
                );
              })}
            </Modal>
          </View>

          <View>
            <TouchableHighlight onPress={() => {
              this.setModalVisible(true)
            }}>
              {this.state.toAccount ?
                <View style={{borderBottomColor: '#d5d0d4', borderBottomWidth: 1, marginBottom: 10}}>
                  <Text style={styles.baseText}>To:</Text>
                  <Text style={[styles.baseText, {fontSize: 18}]}>{this.state.toAccount.name}</Text>
                  <Text style={[styles.baseText, {fontSize: 18}]}>{this.state.toAccount.number}</Text>
                </View>
              :
                <View style={{borderBottomColor: '#d5d0d4', borderBottomWidth: 1, marginBottom: 10}}>
                  <Text style={styles.baseText}>To:</Text>
                  <Text style={[styles.baseText, {fontSize: 18}]}>Select account</Text>
                </View>
              }
            </TouchableHighlight>

            <View style={{borderBottomColor: '#d5d0d4', borderBottomWidth: 1, marginBottom: 10, flexDirection: 'row', alignItems: 'center'}}>
              <Text style={styles.baseText}>Amount £</Text>
              <TextInput
                keyboardType="numeric"
                maxLength={6}
                enablesReturnKeyAutomatically={true}
                autoCorrect={false}
                style={{marginLeft: 10, height: 40, flex: 3}}
                value={this.state.amount}
                onChange={(event) => this.setAmount(event.nativeEvent.text)}
                onSubmitEditing={(event) => this.setAmount(event.nativeEvent.text)}
              />
            </View>
            <Button
              containerStyle={buttonContainerStyle}
              style={styles.button}
              disabled={this.state.buttonDisabled}
              onPress={() => this.showLock()}>
            Make transfer
            </Button>
          </View>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {this.renderTransfer()}
        {this.renderMainView()}
      </View>
    );
  }

  renderBalance(balance) {
    return `£${balance.toFixed(2)}`;
  }

  getAccountHeader(account) {
    const { name, number, sort_code } = account;
    let accountHeader = number;

    if (sort_code) {
      accountHeader = accountHeader + ' | ' + sort_code;
    }
    if (name) {
      accountHeader = accountHeader + ' | ' + name;
    }
    return accountHeader;
  }

  renderHeader() {
    const { account } = this.props;
    const accountHeader = this.getAccountHeader(account);
    return (
      <View style={styles.header}>
        <Text style={styles.baseText}>From:</Text>
        <Text style={[styles.baseText, styles.accountType]}>{account.type}</Text>
        <Text style={styles.baseText}>{accountHeader}</Text>
        <Text style={[styles.baseText, styles.balance]}>{this.renderBalance(account.current_balance)}</Text>
        <Text style={[styles.baseText, styles.availableBalance]}>{this.renderBalance(account.available_balance)} available</Text>
      </View>
    );
  }
}

Transfer.propTypes = {
  account: PropTypes.object.isRequired,
};

export default Transfer;
