import React, { PropTypes } from 'react';
import ReactNative from 'react-native';
var {
  Text,
  View,
  StyleSheet,
  TouchableHighlight
} = ReactNative;
import AccountOverview from './AccountOverview';
import { Actions } from "react-native-router-flux";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 63,
    backgroundColor: '#efeaee'
  },
  overview: {
    borderBottomColor: '#d5d0d4',
    borderBottomWidth: 1,
    paddingBottom: 40
  },
  listItemContainer: {
    flex: 3,
  },
  listItem: {
    padding: 20,
    borderBottomColor: '#d5d0d4',
    borderBottomWidth: 1
  },
  link: {
    color: '#00a6cc',
    fontWeight: 'bold',
    fontSize: 14,
  },
  baseText: {
    fontFamily: 'Lato',
    color: '#564c53'
  }
});

import firebaseApp from '../util/firebaseApp';

class AccountInfo extends React.Component {

  listenForAccount(selectedAccountRef) {
    selectedAccountRef.on('value', (snap) => {
      const account = {
        type: snap.val().type,
        name: snap.val().name,
        number: snap.val().number,
        sort_code: snap.val().sort_code,
        current_balance: snap.val().current_balance,
        available_balance: snap.val().available_balance,
      };

      this.setState({
        selectedAccount: account
      });
    });
  }

  componentWillMount() {
    const { accountNumber } = this.props;
    this.selectedAccountRef = firebaseApp.database().ref().child(`accounts/aielondon/${accountNumber}`);
    this.listenForAccount(this.selectedAccountRef);
  }

  componentWillUnmount() {
    this.selectedAccountRef.off();
  }

  render() {
    const { selectedAccount } = this.state;
    const { type } = selectedAccount;
    const onTransactions = () => {
      Actions.transactions({account: selectedAccount});
    };
    const onTransfer = () => {
      Actions.transfer({account: selectedAccount});
    };

    return (
      <View style={styles.container}>
        <View style={styles.overview}>
          <AccountOverview account={selectedAccount} />
        </View>
        <View style={styles.listItemContainer}>
          <TouchableHighlight onPress={onTransactions} style={styles.listItem}>
            <View>
              <Text style={styles.link}>View Transactions</Text>
            </View>
          </TouchableHighlight>
        { type && (type == 'Current account' || type == 'Savings account') &&
          <TouchableHighlight onPress={onTransfer} style={styles.listItem}>
            <View>
              <Text style={styles.link}>Transfer between accounts</Text>
            </View>
          </TouchableHighlight>
        }
        </View>
      </View>
    );
  }
}

AccountInfo.propTypes = {
  accountNumber: PropTypes.number.isRequired
};

export default AccountInfo;
