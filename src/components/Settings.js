import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import Auth0Lock from 'react-native-lock';

import credentials from '../../auth0-credentials';
let lock = new Auth0Lock(credentials);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#efeaee'
  },
  description: {
    fontSize: 20,
    textAlign: 'center',
    color: '#564c53'
  }
});

class Settings extends React.Component {

  showLock() {
    lock.show({
      closable: true,
      authParams: {
        scope: 'step_up',
      },
      connections: ["sms"],
    }, (err, profile, token) => {
      if (err) {
        console.log(err);
        return;
      }
      //Actions.Tabbar({ profile: profile });
      console.log('stepped up!');
    });
  }

  render() {
    const { email, name } = this.props.profile;
    return (
      <View style={styles.container}>
        <Text style={styles.description}>Welcome {name}!</Text>
        <Text style={styles.description}>Your email is: {email}</Text>
      </View>
    );
  }
}

Settings.propTypes = {
  profile: PropTypes.object,
};

export default Settings;
