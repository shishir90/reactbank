import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ListView,
} from 'react-native';
import firebaseApp from '../util/firebaseApp';
import moment from 'moment';
import _ from 'lodash';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 63,
    backgroundColor: '#efeaee'
  },
  name: {
    marginLeft: 10,
  },
  amount: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  row: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-start',
    borderBottomColor: '#d5d0d4',
    borderBottomWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  baseText: {
    fontFamily: 'Lato',
    color: '#564c53'
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    borderBottomColor: '#d5d0d4',
    borderBottomWidth: 1
  },
  headerLeft: {
    flex: 2,
    flexDirection: 'column'
  },
  headerRight: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 20
  }
});

class Transactions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      })
    };
  }

  listenForTransactions(transactionsRef) {
    transactionsRef.on('value', (snap) => {
      // get children as an array
      let items = [];
      snap.forEach((child) => {
        items.push({
          amount: child.val().amount,
          date: child.val().date,
          name: child.val().name,
          reference: child.val().reference,
          type: child.val().type,
          transactionType: child.val().transactionType,
          _key: child.key
        });
      });

      // sort the transactions by date in reverse (so newest at the top).
      items = _.reverse(_.sortBy(items, ['date']))
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items)
      });
    });
  }

  componentWillMount() {
    const { number } = this.props.account;
    this.transactionsRef = firebaseApp.database().ref().child(`transactions/${number}`);
    this.listenForTransactions(this.transactionsRef);
  }

  componentWillUnmount() {
    this.transactionsRef.off();
  }

  render() {
    const { account } = this.props;
    return (
      <View style={styles.container}>
        {this.renderHeader(account)}
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderTransaction.bind(this)}
          enableEmptySections={true}
          style={{flex:4}} />
      </View>
    );
  }

  renderTransactionAmount(type, amount) {
    if (type == 'debit') {
      return `-£${amount.toFixed(2)}`;
    }
    else {
      return `£${amount.toFixed(2)}`;
    }
  }

  renderBalance(balance) {
    return `£${balance.toFixed(2)}`;
  }

  renderHeader(account) {
    return (
      <View style={styles.header}>
        <View style={styles.headerLeft}>
          <Text style={[styles.baseText, {fontSize: 18}]}>{account.type}</Text>
          <Text style={[styles.baseText, {fontSize: 18}]}>{account.number}</Text>
          <Text style={[styles.baseText, {fontSize: 18}]}>{account.name}</Text>
        </View>
        <View style={styles.headerRight}>
          <Text style={[styles.baseText , {fontSize: 26} ]}>{this.renderBalance(account.available_balance)}</Text>
        </View>
      </View>
    );
  }

  renderTransaction(transaction) {
    const { transactionType, amount, date, name } = transaction;
    return (
      <View style={styles.row}>
        <Text style={styles.baseText}>{this.formatShortDate(date)}</Text>
        <Text style={[styles.baseText, styles.name]}>{name.toUpperCase()}</Text>
        <View style={styles.amount}>
          <Text style={styles.baseText}>{this.renderTransactionAmount(transactionType, amount)}</Text>
        </View>
      </View>
    );
  }

  formatShortDate(date) {
    const momentDate = moment(date);
    return momentDate.format("DD MMM");
  }
}

Transactions.propTypes = {
  account: PropTypes.object.isRequired,
};

export default Transactions;
