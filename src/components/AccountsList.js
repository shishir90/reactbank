import React from 'react';
import {
  View,
  StyleSheet,
  ListView,
} from 'react-native';
import AccountOverview from './AccountOverview';
import { Actions } from "react-native-router-flux";
import firebaseApp from '../util/firebaseApp';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 63,
    backgroundColor: '#efeaee'
  },
  list: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-start',
    borderBottomColor: '#d5d0d4',
    borderBottomWidth: 1
  },
});

class AccountsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      })
    };
  }

  listenForAccounts(accountsRef) {
    accountsRef.on('value', (snap) => {
      // get children as an array
      var items = [];
      snap.forEach((child) => {
        items.push({
          type: child.val().type,
          name: child.val().name,
          number: child.val().number,
          sort_code: child.val().sort_code,
          current_balance: child.val().current_balance,
          available_balance: child.val().available_balance,
          _key: child.key
        });
      });

      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items)
      });
    });
  }

  componentWillMount() {
    // @todo - replace with dynamic lookup based on user email address.
    // const { email } = this.props.profile;
    // need to remove the . in the email.
    this.accountsRef = firebaseApp.database().ref().child('accounts/aielondon');
    this.listenForAccounts(this.accountsRef);
  }

  componentWillUnmount() {
    this.accountsRef.off();
  }

  render() {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderAccount.bind(this)}
          enableEmptySections={true}
          style={styles.list} />
      </View>
    );
  }

  renderAccount(account) {
    const onPress = () => {
      Actions.account_info({accountNumber: account.number});
    };

    return (
      <View style={styles.row}>
        <AccountOverview account={account} onPress={onPress} />
      </View>
    );
  }
}

export default AccountsList;
