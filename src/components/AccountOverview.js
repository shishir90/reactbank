import React from 'react';
import { PropTypes } from 'react';
import {
  TouchableHighlight,
  Text,
  View,
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex:1,
    padding: 10,
  },
  accountType: {
    fontSize: 20,
    paddingBottom: 5,
  },
  balance: {
    fontSize: 20,
    marginTop: 25,
  },
  availableBalance: {
    fontSize: 14,
    marginTop: 5,
  },
  baseText: {
    fontFamily: 'Lato',
    color: '#564c53'
  }
});

class AccountOverview extends React.Component {

  _getAccountHeader(account) {
    const { name, number, sort_code } = account;
    let accountHeader = number;

    if (sort_code) {
      accountHeader = accountHeader + ' | ' + sort_code;
    }
    if (name) {
      accountHeader = accountHeader + ' | ' + name;
    }
    return accountHeader;
  }

  renderBalance(balance) {
    return `£${balance.toFixed(2)}`;
  }

  render() {
    const { type, current_balance, available_balance } = this.props.account;
    const accountHeader = this._getAccountHeader(this.props.account);

    return (
      <TouchableHighlight onPress={this.props.onPress} style={styles.container}>
        <View>
          <Text style={[styles.baseText, styles.accountType]}>{type}</Text>
          <Text style={styles.baseText}>{accountHeader}</Text>

          <Text style={[styles.baseText, styles.balance]}>{this.renderBalance(current_balance)}</Text>
          <Text style={[styles.baseText, styles.availableBalance]}>{this.renderBalance(available_balance)} available</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

AccountOverview.propTypes = {
  account: PropTypes.object.isRequired,
  onPress: PropTypes.func,
};

export default AccountOverview;
