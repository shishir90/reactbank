import * as firebase from 'firebase';

// Initialize Firebase
// @todo - replace this with external config.
const firebaseConfig = {
  apiKey: "AIzaSyCkBX6030J0EH_xDj4vW8BRP4sfw6VdHhA",
  authDomain: "aie-bank.firebaseapp.com",
  databaseURL: "https://aie-bank.firebaseio.com",
  storageBucket: "aie-bank.appspot.com",
};
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp;