import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import AccountsList from './components/AccountsList';
import AccountInfo from './components/AccountInfo';
import Transactions from './components/Transactions';
import GetCash from './components/GetCash';
import Payment from './components/Payment';
import Transfer from './components/Transfer';
import Settings from './components/Settings';
import Login from './components/Login';
import TabIcon from './components/TabIcon';
import {
  StyleSheet
} from 'react-native';
import CodePush from "react-native-code-push";
let codePushOptions = {
  installMode: CodePush.InstallMode.ON_NEXT_RESUME,
  checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME
};

const styles = StyleSheet.create({
  tabBar: {
    borderTopWidth: 1,
    borderTopColor: '#8b3171',
    backgroundColor: 'white',
    height: 70
  }
});

class Root extends React.Component {
  componentDidMount() {
    CodePush.sync(codePushOptions);
  }

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="login" component={Login} title="Login" initial={true} />
          <Scene key="Tabbar" tabs={true} default="accounts" tabBarStyle={styles.tabBar} >
            <Scene key="accounts" title="Accounts" icon={TabIcon} iconName={"account-circle"}>
              <Scene key="accounts_overview" component={AccountsList} title="Accounts" />
              <Scene key="account_info" component={AccountInfo} title="Account Details" backTitle="Back" />
              <Scene key="transactions" component={Transactions} title="Transactions" backTitle="Back" />
              <Scene key="transfer" component={Transfer} title="Transfer between accounts" backTitle="Back" />
            </Scene>
            <Scene key="payment" component={Payment} title="Make Payment" icon={TabIcon} iconName={"payment"} />
            <Scene key="cash" component={GetCash} title="Get Cash" icon={TabIcon} iconName={"attach-money"} />
            <Scene key="settings" component={Settings} title="Settings" icon={TabIcon} iconName={"settings"} />
          </Scene>
        </Scene>
      </Router>
    );
  }
}

export default Root;
