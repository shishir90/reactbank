import React from 'react-native';
import Root from './src/root';

const {
  AppRegistry
} = React;

AppRegistry.registerComponent('rnFirebaseAuth0', () => Root);