/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"
#import "CodePush.h"

#import "RCTBundleURLProvider.h"
#import "RCTRootView.h"
#import "A0Theme.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation;

  
#ifdef DEBUG
    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
#else
    jsCodeLocation = [CodePush bundleURL];
#endif

  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"rnFirebaseAuth0"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  [self customizeLockTheme];
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];

  return YES;
}

- (void)customizeLockTheme {
  A0Theme *theme = [[A0Theme alloc] init];

  [theme registerImageWithName: @"logo"
                             bundle: [NSBundle mainBundle]
                             forKey: A0ThemeIconImageName];

  [theme registerColor: [UIColor whiteColor] forKey: A0ThemeIconBackgroundColor];
  [theme registerColor: [UIColor whiteColor] forKey: A0ThemeTitleTextColor];
  [theme registerColor: [UIColor whiteColor] forKey: A0ThemeSecondaryButtonTextColor];
  [theme registerColor: [UIColor whiteColor] forKey: A0ThemeTextFieldTextColor];
  [theme registerColor: [UIColor whiteColor] forKey: A0ThemeDescriptionTextColor];
  [theme registerColor: [self plumColor] forKey: A0ThemePrimaryButtonNormalColor];

  [theme registerFont: [UIFont fontWithName:@"Lato-Regular" size:20] forKey: A0ThemeTitleFont];
  [theme registerFont: [UIFont boldSystemFontOfSize: 20] forKey: A0ThemePrimaryButtonFont];
  [theme registerImageWithName: @"background"
                        bundle: [NSBundle mainBundle]
                        forKey: A0ThemeScreenBackgroundImageName];

  [[A0Theme sharedInstance] registerTheme: theme];
}

- (UIColor *) plumColor {
  return [UIColor colorWithRed:0.55 green:0.19 blue:0.44 alpha:1.0];
}

@end
