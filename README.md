# React Native AIE Banking example

An example native banking application built with React Native

Includes the following -

- React Native
- Auth0 (for Login/Authentication)
- Firebase (for Database)


## Getting started

- Install React Native and it's dependencies. Follow the [getting started guide](https://facebook.github.io/react-native/docs/getting-started.html)

Then run:
```

cd react-native-bank
npm install
react-native link
```

Running the application (only tested on iOS):

```
react-native run-ios
```

## Importing sample data to Firebase

Install [firebase-import](https://github.com/firebase/firebase-import):

```
npm install -g firebase-import
```

Import the data:

```
firebase-import -f https://FIREBASEAPP.firebaseio.com -j tests/sample-data.json
```